# Suckless Dotfiles
Simple dotfiles designed with productivity and simplicity in mind. Perhaps compiling multiple programs in C isn't so simple, but at least these defaults are. All color schemes are automatically configured with Pywal in `.config/wal/`. A color scheme can be generated with `wal -i "path/to/dir"` where the path represents the wallpaper parameter. Input Mono is used as the primary font at 14px to maximize readability. I don't have 20/20 vision. 
## Software 
Detailed [notes](https://lzrd.dev/notes/ricing.html) on setting up specific software can be found at the memex. These configuration files are designed to work with the following:
- dwm
- st
- dmenu
- slstatus (todo)
## Installation
Suckless programs will need to be compiled from source. Go into the directory of the program you want to install and run `make clean install`. If you want everything in the bundle then run the setup script `./deploy.sh`. Restart your X server.



